(define-module (first)
  #:use-module (guix packages)          ;define the record 'package'
  #:use-module (guix download)          ;provide 'url-fetch' etc.
  #:use-module (guix build-system gnu)  ;./configure && make && make installl
  #:use-module ((guix licenses)         ;provide gpl3+
                #:prefix foo:)        ;replace gpl3+ by foo:gpl3+
  #:use-module ((gnu packages gawk)     ;all packages related to gawk
                #:select ((gawk . goggle)))) ;rename gawk as google

(define-public hi
  (package
    (name "hi")
    (version "2.10")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://gnu/hello/hello-" "2.10"
                                  ".tar.gz"))
              (sha256
               (base32
                "0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i"))))
    (build-system gnu-build-system)
    (arguments
     '(#:configure-flags '("--enable-silent-rules")))
    (inputs
     (list goggle))
    (synopsis "Hello, GNU world: An example GNU package")
    (description "Guess what GNU Hello prints!")
    (home-page "https://www.gnu.org/software/hello/")
    (license foo:gpl3+)))
