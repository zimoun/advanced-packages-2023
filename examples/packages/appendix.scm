(define-module (appendix)
  #:use-module (guix packages)
  #:use-module (gnu packages base)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (gnu packages emacs))

(define-public bye
  (package
    (inherit hello)
    (name "bye")
    (arguments
     (list
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'install 'do-something-with-emacs
            (lambda _
              (invoke #$(file-append emacs-minimal
                                     "/bin/emacs") "--version"))))))))
