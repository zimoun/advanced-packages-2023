#!/usr/bin/env -S guix repl -q --
;; -*- mode: scheme -*-
!#
;;; Copyright © 2023 Simon Tournier <zimon.toutoune@gmail.com>
;;;
;;;
;;; This script illustrates how G-expression, derivation and Guix builder are
;;; related to.
;;;
(use-modules (guix gexp)
             (guix derivations)
             (guix store)

             ((ice-9 textual-ports) #:select (get-string-all))
             (ice-9 match))

;;;
;;; Helpers
;;;

(define (number->name n)
  (string-append "Fibonacci-of-"
                 (number->string n)))

(define (number->gexp n)
  #~(begin
      (use-modules (ice-9 format))
      (call-with-output-file #$output
        (lambda (port)
          (format port "~d" #$n)))))

(define (store-item->number path)
  #~(begin
      (use-modules ((ice-9 textual-ports) #:select (get-string-all)))
      (string->number
       (call-with-input-file #$path
         get-string-all))))

;;;
;;; Core
;;;

(define (compute n)
  (if (or (= 0 n) (= 1 n))
      (gexp->derivation
       (number->name n)
       (number->gexp n))
      (let* ((store (open-connection))
             (drv-1 (run-with-store store
                       (compute (- n 1))))
             (drv-2 (run-with-store store
                       (compute (- n 2))))
             (f_1 (store-item->number drv-1))
             (f_2 (store-item->number drv-2)))
        (gexp->derivation
         (number->name n)
         (number->gexp #~(+ #$f_1 #$f_2))))))


;;;
;;; Entry point
;;;
(let* ((n (match (command-line)
                     ;; Parse command line
                     ((_ x)
                      (catch #t         ;catch all errors
                        (lambda ()      ;try
                          (string->number x))
                        (lambda args    ;raise
                          (begin
                            (format #t "Wrong invokation.  Just feed with an integer.")
                            (exit 1)))))
                     (_
                      (begin
                        (format #t "Error.  Try: ./fibonacci.scm 7")
                        (exit 1)))))
       (store (open-connection))
       (drv (run-with-store store
              (compute n)))
       (result (derivation->output-path drv)))
  (begin
    (build-derivations store (list drv))
    (format #t "~a~%" result)
    (format #t "= ~a~%"
            ;; Read the result
            (call-with-input-file result get-string-all))
    (exit 0)))
