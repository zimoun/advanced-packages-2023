#!/usr/bin/env -S guix repl -q --
;; -*- mode: scheme -*-
!#
;;; Copyright © 2023 Simon Tournier <zimon.toutoune@gmail.com>
;;;
;;;
;;; This script shows difference of phases between two build-systems.
;;;
;;; ./compare-bs-phases.scm gnu python
;;;;

(use-modules
 ;;;;
 ;;;; Helpful from interactive "guix repl"
 ;;;;
 ;; ((guix build gnu-build-system)
 ;;  #:select ((%standard-phases . standard-phases))  ;select and rename
 ;;  #:prefix gnu:)

 ;; ((guix build python-build-system)
 ;;  #:select ((%standard-phases . standard-phases))
 ;;  #:prefix python:)

 (ice-9 match)
 ((srfi srfi-1) #:select (lset-difference
                          lset-intersection)))



;;;
;;; Helpers
;;;

(define (list-phases standard-phases)
  (map
   (match-lambda
     ((phase . procedure)
      phase))
   standard-phases))

(define (argument->standard-phases str)
  (let ((bs (string->symbol
             (string-append str "-build-system"))))
    (catch #t                           ;catch all errors
      (lambda ()                        ;try
        (list-phases
         (module-ref (resolve-interface `(guix build ,bs))
                     '%standard-phases)))
      (lambda args                      ;raise
        (begin
          (format #t "Error when loading module ~s... " str)
          (format #t "return *EMPTY* list instead~%")
          '())))))


;;;
;;; Entry point
;;;

(match (command-line)
  ((progn-name arg1 arg2)
   (let* ((phases-1 (argument->standard-phases arg1))
          (phases-2 (argument->standard-phases arg2))
          (only-1 (lset-difference eq? phases-1 phases-2))
          (only-2 (lset-difference eq? phases-2 phases-1)))
     (begin
       (format #t "~%Compare %standard-phases of ~s and ~s:~%" arg1 arg2)
       (format #t "  ~d phases in ~s~%" (length phases-1) arg1)
       (format #t "  ~d phases in ~s~%" (length phases-2) arg2)
       (format #t "  ~d phases in common~%" (length
                                             (lset-intersection
                                              eq? phases-1 phases-2)))
       (newline)
       (format #t "Only in ~s:~% ~{+ ~a~% ~}~%" arg1 only-1)
       (format #t "Only in ~s:~% ~{+ ~a~% ~}~%" arg2 only-2)
       (exit 0))))
  (_
   (begin
     (format #t "Error.  Try: ./compare-bs-phases.scm gnu python")
     (exit 1))))
