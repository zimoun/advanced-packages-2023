#!/usr/bin/env -S guix repl -q --
;; -*- mode: scheme -*-
!#
;;; Copyright © 2023 Simon Tournier <zimon.toutoune@gmail.com>
;;;
;;;
;;; This script show URL of packages.
;;;

(use-modules
 (ice-9 match)

 ((gnu packages) #:select (specification->package))
 (guix packages)

 ((web uri) #:select (string->uri
                      uri->string))
 ((guix build download) #:select (maybe-expand-mirrors))
 ((guix download) #:select (%mirrors))
 (guix git-download))

(let ((pkgs (match (command-line)
              ((prog-name pkgs ...)
               (map specification->package pkgs))
              (_
               (begin
                 (format #t "Error. Try: ./show-me-url.scm hello")
                 (exit 1))))))
  (for-each
   (lambda (pkg)
     (match (package-source pkg)
       ;; See 'origin->json' from file build-package-metadata.scm in repository
       ;; maintenance.git for an exhaustive case handling.
       ((? origin? o)
        (match (origin-uri o)
          ((? string? s)
           ;; Pick only the first (car) of the potenial long list
           (let ((url (car (map uri->string
                                (maybe-expand-mirrors (string->uri s) %mirrors)))))
             (format #t "wget      ~a~%" url)))
          ((? git-reference? g)
           (let* ((url (git-reference-url g))
                 (revision (git-reference-commit g))
                 (tmpdir (string-append "/tmp/" (package-name pkg)))
                 (git (string-append "git -C " tmpdir)))
             (format #t "mkdir -p ~a ~%" tmpdir)
             (format #t "&& ~a init --initial-branch=main ~%" git)
             (format #t "&& ~a remote add origin ~a ~%" git url)
             (format #t "&& ~a fetch --depth 1 origin ~a ~%" git revision)
             (format #t "&& ~a checkout FETCH_HEAD ~%" git)))
          ;; All the other cases
          (_ #f)))))
   pkgs)
  (exit 0))
