#!/usr/bin/env bash

here=$(pwd)
cd src
if [ -f support.snm ]
then
    guix time-machine -C channels.scm \
         -- shell -C -m manifest.scm  \
         -- rubber --pdf note.tex
fi
cd $here

if [ ! -f src/support.snm ]
then
    echo Missing src/support.snm: Clean and Redo all...
    ./redo-all.sh
fi
