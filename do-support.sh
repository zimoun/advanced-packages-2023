#!/usr/bin/env bash

here=$(pwd)
cd src
guix time-machine -C channels.scm   \
       -- shell -C -m manifest.scm  \
       -- rubber --pdf support.tex
cd $here
