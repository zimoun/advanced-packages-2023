#!/usr/bin/env bash

./do-clean.sh
./do-support.sh
./do-note.sh

here=$(pwd)
cd src
for f in support note
do
    guix time-machine -C channels.scm   \
         -- shell -C -m manifest.scm    \
         -- rubber --pdf ${f}.tex --force
done
cd $here
