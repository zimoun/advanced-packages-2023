(specifications->manifest
 (list
  "rubber"

  "texlive-scheme-basic"
  "texlive-ec"
  "texlive-kpfonts"
  "texlive-cm-super"
  "texlive-amsfonts"

  "texlive-beamer"
  "texlive-translator"
  "texlive-ulem"
  "texlive-capt-of"
  "texlive-hyperref"
  "texlive-carlisle"

  "texlive-wrapfig"
  "texlive-amsmath"
  "texlive-listings"
  "texlive-pgf"

  "texlive-geometry"
  "texlive-textpos"
  ;"texlive-babel-french"
  "texlive-fancyvrb"
  "texlive-fancyhdr"
  ))
