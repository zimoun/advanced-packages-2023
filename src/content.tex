\mode*        %Follow up of Beamer configuration when using ignorenonframetext.

\begin{abstract}
  This tutorial is dedicated to review what can be done when that’s not enough
  to list dependencies and/or declare a build system.  The aim is to introduce
  various mechanisms for adapting the base Guix recipe.  The prerequisite is
  the reading of the section “Defining Packages” from the manual and the goal
  of this tutorial is to provide some ingredients for making it sound.  We
  propose to first introduce a Scheme/Guile Swiss-knife toolbox, then to cover
  how to modify upstream source code (field \texttt{origin}) and how to
  customize the build system parameters or phases (field \texttt{arguments}).
  If time allows, we will introduce the meaning of cryptic symbols as the
  sequence \texttt{\#\~{}(\#\$(}%.

  \medskip

  Do not forget that packaging is a craft, so there is \textbf{no magic but
    only practise.}
\end{abstract}

\begin{center}
  (Terms using \textsf{sans serif} font are hyperlinks.)
\end{center}

\indent

This tutorial for introducing key components to make advanced packages for
Guix is its first version.  Do not take all as written in stone; it is based
on my experience and I do not consider being a packaging expert.  If I might,
my two only advises are:

\begin{enumerate}
\item Dive into existing packages and confront with Guix manual.
\item Most is about \textbf{a lot of} practise.  Quoting \textsf{rekado},

  \begin{flushright}
    I wish I had anything to say about this other than

    \emph{“try again, give
    up, forget about it, remember it, ask for pointers, repeat”}

  \href{https://logs.guix.gnu.org/guix-hpc/2023-10-13.log#142159}%
  {\textsf{\texttt{\#guix-hpc} on 2023-10-13}}.
  \end{flushright}
\end{enumerate}



\begin{frame}<presentation>[plain, noframenumbering]{Plan for today}
  \begin{center}
    \textbf{A pedestrian journey toward G-expressions and Schemey-way}
  \end{center}

  \vfill
  \begin{center}
    \begin{minipage}{0.7\textwidth}
      \begin{itemize}
      \item The aim is to provide some ``helpers'',
      \item For easing the reading of Guix manual and source code.
      \end{itemize}
    \end{minipage}
  \end{center}

  \vfill
  \setcounter{tocdepth}{2}
  \tableofcontents
\end{frame}



\section{Introduction}

As a warmer, let re-read the section
\hrefsf{https://guix.gnu.org/manual/devel/en/guix.html\#Defining-Packages}%
{Defining Packages} from the Guix manual.  The aim of this $\approx 1h30$
tutorial is to help in packaging when
\hrefsf{https://guix.gnu.org/manual/devel/en/guix.html\#Invoking-guix-import}%
{Invoking \texttt{guix import}} is not enough.


\thisslide{preliminary-1}
\begin{frame}<presentation>[fragile, label=preliminary-1]{Preliminary I
  \hfill how to connect using \texttt{ssh} on VM}
  \begin{enumerate}
  \item  Local file: \texttt{\~{}/.ssh/config}
\begin{verbatim}
Host bastion
     Hostname repro-guix.u-ga.fr
     User user_1   # or user_2 or user_3 or user_4
     StrictHostKeyChecking no
     IdentityFile ~/.ssh/stournier

Host repro-guix*
     User user_1  # same as previously
     ProxyCommand ssh bastion -W %h:22
\end{verbatim}
  \end{enumerate}
\end{frame}


\begin{frame}<presentation>[fragile, label=preliminary-1]{Preliminary I bis
    \hfill how to connect using \texttt{ssh} on VM}
  \begin{enumerate}
  \item Create a SSH key for the session (here \code{\~{}/.ssh/stournier}, pick
    your name)
\begin{verbatim}
ssh-keygen -t rsa -b 4096 -f ~/.ssh/stournier
\end{verbatim}
  \item Copy the public SSH key
\begin{verbatim}
cat ~/.ssh/stournier.pub | ssh bastion 'cat >> .ssh/authorized_keys'
\end{verbatim}
  \item Start your editor (VSCode or Emacs via Tramp or else)
  \end{enumerate}

  \vfill
  Or just connect to one VM, then start Emacs in text mode:
  \begin{center}
    \texttt{guix shell emacs-minimal -{}- emacs -f shell}
  \end{center}
\end{frame}



\thisslide{preliminary-2}
\begin{frame}<presentation>[fragile, label=preliminary-2]{Preliminary II}
  \begin{enumerate}
  \item Clone Git repository:
    \href{https://gitlab.com/zimoun/advanced-packages-2023}%
    {\texttt{https://gitlab.com/zimoun/advanced-packages-2023}}
  \item Start a terminal
  \end{enumerate}
  \vfill
  \begin{center}
    \begin{tabular}{l}
      \code{\$ guix show -L examples/packages hi}
      \\ \\
      \code{\$ guix build -L examples/packages hi}
      \\
      \code{\$ guix build -L examples/packages hi -{}-no-grafts -{}-check -K}
      \\ \\
      \code{\$ guix edit -L \$(pwd)/examples/packages hi}
    \end{tabular}
  \end{center}

  \vfill
  \begin{block}{}
    \emph{workaround} \uline{VSCode}: \code{EDITOR=./vscode-wrapper guix edit}
    %% VSCode: export XDG_DATA_DIRS=/usr/share:$XDG_DATA_DIRS
  \end{block}
  Plain text: \texttt{EDITOR=less guix edit}
\end{frame}


\thisslide{def-pkg}
\begin{frame}<presentation>[fragile, label=def-pkg]%
  {Defining Packages: key points
    \hfill file: \texttt{examples/packages/first.scm}}
  \begin{description}
  \item[define-module] Create a Guile module
    \begin{description}
    \item[\#:use-module] List the modules required for Guile \emph{compiling} the
      recipe
    \end{description}
  \item[define-public] Define and export
    \begin{description}
    \item[package] Object representing a package (Scheme record)
      \begin{description}
      \item[name] The string we prefer
      \item[version] A string that makes sense
      \item[source] Define where to fetch the source code
      \item[build-system] Define how to build
      \item[arguments] The arguments for the build system
      \item[inputs] List the other package dependencies
      \end{description}
    \end{description}
  \end{description}
  \begin{block}{}
    \begin{center}
      \texttt{guix repl -L examples/packages}
    \end{center}
  \end{block}
\end{frame}



\thisslide{pkg-repl}
\begin{frame}<presentation>[fragile, label=pkg-repl]{Package from \texttt{guix repl}}

  \begin{block}{}
    \begin{center}
      Recommendation for the file \texttt{\~{}/.guile}
    \end{center}
\begin{verbatim}
(use-modules (ice-9 readline)       ;; package guile-readline, guile?
             (ice-9 format)
             (ice-9 pretty-print))
(activate-readline)
\end{verbatim}
  \end{block}

  \begin{enumerate}
  \item Type \texttt{hi} then \texttt{,q}
  \item Type \texttt{(use-modules (first))} (or \texttt{,use(first)}) and again \texttt{hi}
  \item Try \texttt{(package-name hi)} then \texttt{,use(guix packages)} (or
    \texttt{,use(guix)}) and repeat
  \end{enumerate}

  \begin{center}
    \textbf{Two names: the Scheme variable and the string.}
  \end{center}

  \begin{enumerate}
  \item How to display the version?
  \item Try \texttt{(package-inputs hi)}
  \end{enumerate}
\end{frame}

Quoting
\hrefsf{https://guix.gnu.org/manual/devel/en/guix.html\#Package-Naming}%
{Package Naming}:

\emph{%
  A package actually has two ``names'' associated with it.  First, there is
  the name of the Scheme variable, the one following ‘define-public’.  By
  this name, the package can be made known in the Scheme code, for instance as
  input to another package.  Second, there is the string in the ‘name’ field
  of a package definition.  This name is used by package management commands
  such as ‘guix package’ and ‘guix build’.
}

Pick the same for both is welcome but not mandatory.  And several packages for
the same version have the same string name but not the same symbol; except if
they are defined in different modules.


\clearpage
\section{Scheme/Guile Swiss-knife toolbox}


\thisslide{intro-scheme}
\begin{frame}<presentation>[fragile, label=intro-scheme]{Examples of packages}
\begin{verbatim}
$ guix edit gsl
$ guix edit r-torch
\end{verbatim}

  \vfill
  \begin{center}
    What does it mean?
  \end{center}
  \vfill

  \begin{description}
  \item[keyword] \code{define-public}, \code{let}, \code{lambda}
  \item[record] \code{package}
  \item[convention] \code{\%something}, \code{something?}, \code{something*}
  \item[symbol] quote (\code{'}), backtick (\code{\`{}}), comma (\code{,}),
    comma at (\code{,@}), underscore (\code{\_})\\
    G-expressions: \code{\#\~{}} or \code{\#\$}
  \end{description}
\end{frame}


\thisslide{first-thing-first}
\begin{frame}<presentation>[fragile, label=first-thing-first]{First things first}
  \begin{center}
  S-expression: atom or expression of the form \code{(x y …)}
  \end{center}

  \begin{description}
  \item[atom:] +, *, \code{list}, etc.
  \item[expression:] \code{(list 'one 2 "three")}
  \end{description}

  \visible<2->{
  \begin{block}{}
  Call (evaluation) with parenthesis

  \hfill e.g., apply the atom \code{list} to the rest

  \hfill \code{(list 'one 2 "three")} returns the list composed by the
  elements \code{(one 2 "three")}
  \end{block}
  }

  \vfill

  \visible<3->{
  \begin{exampleblock}{}
  Quote protects from the call (do not evaluate)

  \hfill e.g., \code{'one} returns plain \code{one}

  \hfill e.g., \code{'(list one 2 "three")} returns
  \code{(list 'one 2 "three")}

  \hfill
  \code{'(list 'one 2 "three")} returns \code{(list (quote one) 2 "three")}
  \end{exampleblock}
  }
\end{frame}

"quoted" data remains unevaluated, and provides a convenient way of
representing Scheme programs. This is one of the big payoffs of Lisp's simple
syntax: since programs themselves are lists, it is extremely simple to
represent Lisp programs as data. Compare the simplicity of quoted lists with
the ML datatype that we used to represent ML expressions.

This makes it simple to write programs that manipulate other programs --- it
is easy to construct and transform programs on the fly.

Note that names in Lisp programs are translated into symbols in quoted Lisp
expressions. This is so that quoted names can be distinguished from quoted
strings; consider the difference between the following two expressions:



\thisslide{second-thing}
\begin{frame}<presentation>[fragile, label=second-thing]{Second thing second}
  \begin{description}
  \item[variable] \code{(define some-variable 42)}
  \item[procedure] \code{(lambda (argument) (something argument))}
  \end{description}

  \begin{block}{}
    \begin{center}
      Define a procedure
    \end{center}
\begin{lstlisting}[language=Scheme]{Name=proc}
(define my-name-procedure
  (lambda (argument1 argument2)
    (something-with argument1)))
\end{lstlisting}
\end{block}

  \begin{alertblock}{}
\begin{verbatim}
(define (my-name-procedure argument1 argument2)
  (something-with argument1))
\end{verbatim}
  \end{alertblock}
  Call \code{(my-name-procedure 1 "two")}

  \small
  \begin{center}
  \code{define-public} is sugar to \code{define} and export (see «
  \href{https://www.gnu.org/software/guile/manual/html_node/Creating-Guile-Modules.html}{Creating
    Guile Modules (link)}) »
\end{center}
\end{frame}


\thisslide{def-let}
\begin{frame}<presentation>[fragile, label=def-let]{Local variables \hfill = \code{let}}
  \begin{block}{}
    \begin{center}
      \textbf{Independent local variables}
    \end{center}
\begin{verbatim}
(define (add-plus-2 x y)
  (let ((two 2)
        (x+y (+ x y)))
    (+ x+y two)))
\end{verbatim}
  \end{block}

  \begin{exampleblock}{}
    \begin{center}
      \textbf{Inter-dependant local variables}
    \end{center}
\begin{verbatim}
(define (add-plus-2-bis x y)
  (let* ((two 2)
         (x+two (+ x two))
         (result (+ y x+two)))
    result))
\end{verbatim}
  \end{exampleblock}
\end{frame}



\thisslide{let-pkg}
\begin{frame}<presentation>[fragile, label=let-pkg]{Local variables: example%
    \hfill seen in package \texttt{julia-biogenerics}}
  \begin{exampleblock}{}
    \begin{lstlisting}[language=Lisp]
(define-public julia-biogenerics
  (let ((commit "a75abaf459250e2b5e22b4d9adf25fd36d2acab6")
        (revision "1"))
    (package
      (name "julia-biogenerics")
      (version (git-version "0.0.0" revision commit))
    ...
    \end{lstlisting}
  \end{exampleblock}
\end{frame}



\thisslide{conventions}
\begin{frame}<presentation>[fragile, label=conventions]{Conventions}
  \begin{description}
  \item<1->[predicate] \textbf{ends with question mark (\code{?})}, return boolean
    (\code{\#t} or \code{\#f}\\
    \hfill \uline{note:} \code{\#true} or \code{\#false} works too)

    \hfill e.g., \code{(string-prefix? "hello" "hello-world")}
  \item<2->[variant] \textbf{ends with star mark (\code{*})}
    \hfill e.g., \code{let*}

    \hfill \code{lambda*} more argument
\begin{verbatim}
(lambda* (#:key inputs #:allow-other-keys)
             (setenv "CONFIG_SHELL"
                     (search-input-file inputs "/bin/sh")))
;; seen in package frama-c
\end{verbatim}
  \item<3->[keyword] \textbf{starts with sharp colon (\code{\#:})}

    \hfill e.g., \code{\#:key}, \code{\#:configure-flags}, \code{\#:phases}
  \item<4->[“global“] \textbf{starts with percent (\code{\%})}

    \hfill e.g., \code{\%standard-phases}
  \end{description}
\end{frame}

% XXXX: Overlay seems broken with BeamerArticle.  Only the first one appears
% in article mode, while the output is expected in presentation mode.


\thisslide{quote-and-co}
\begin{frame}<presentation>[fragile, label=quote-and-co]{Quote, quasiquote, unquote}
  \vspace{-0.3cm}
  \begin{description}
  \item[quote] do not evaluate (keep as it is) \hfill quote \code{'}
  \item[quasiquote] unevaluate except escaped \hfill backtick \code{\`{}}
  \item[unquote] evaluate that escaped \hfill coma \code{,}
  \end{description}

  \vspace{-0.1cm}
  \begin{block}{}
    \begin{center}
      \texttt{guix repl}
    \end{center}
    \vspace{-0.3cm}
    \begin{enumerate}
    \item<2-> Type
      \vspace{-0.3cm}
      \begin{lstlisting}[language=Lisp]
        scheme@(guix-user)> (define ho "path/to/ho")
        scheme@(guix-user)> (string-append ho "/bin/bye")
        scheme@(guix-user)> `(string-append ho "/bin/bye")
        scheme@(guix-user)> `(string-append ,ho "/bin/bye")
      \end{lstlisting}
      \vspace{-0.3cm}
    \item<3-> Type
      \vspace{-0.3cm}
      \begin{lstlisting}[language=Lisp]
        scheme@(guix-user)> (eval $4 (interaction-environment))
      \end{lstlisting}
    \end{enumerate}
  \end{block}
  \visible<4>{
  \begin{alertblock}{}
    \begin{center}
      \textbf{construction-time} vs \textbf{eval-time}
    \end{center}
  \end{alertblock}
  }
\end{frame}



\thisslide{quote-and-co-2}
\begin{frame}<presentation>[fragile, label=quote-and-co-2]%
  {Quote, quasiquote, unquote II \hfill splicing}
  \begin{description}
  \item[unquote-splicing] as unquote and insert the elements
    \hfill comma-at \code{,@}

    \medskip

    \hfill the expression must evaluate to a list
  \end{description}

  \vfill
  \begin{block}{}
    \begin{enumerate}
    \item Type
      \begin{lstlisting}[language=Lisp]
scheme@(guix-user)> (define of (list #:vegetable 'tomatoes
                                     #:dessert (list "cake" "pie")))
scheme@(guix-user)> `(more ,@of that)
scheme@(guix-user)> `(more ,of that)
      \end{lstlisting}
    \end{enumerate}
  \end{block}
\end{frame}



\thisslide{quote-and-co-3}
\begin{frame}<presentation>[fragile, label=quote-and-co-3]%
  {Quote, quasiquote, unquote II bis \hfill splicing}

  \begin{lstlisting}[language=Lisp]
    (arguments
      `(,@(package-arguments gsl)
         #:configure-flags (list "--disable-shared")
         #:make-flags (list "CFLAGS=-fPIC")))
    ;; seen in package gsl-static
   \end{lstlisting}

   \vfill
  \begin{block}{}
        \begin{enumerate}
        \item Type
          \begin{lstlisting}[language=Lisp]
 scheme@(guix-user)> ,use(gnu packages maths)
 scheme@(guix-user)> ,pp (package-arguments gsl)
 scheme@(guix-user)> ,pp `(,@(package-arguments gsl)
                            #:configure-flags (list "--disable-shared")
                            #:make-flags (list "CFLAGS=-fPIC"))
          \end{lstlisting}
        \end{enumerate}
      \end{block}
\end{frame}



\thisslide{quote-and-co-3}
\begin{frame}<presentation>[fragile, label=quote-and-co-3]%
  {Quote, quasiquote, unquote III \hfill digression}
  \vspace{-0.2cm}
  \begin{exampleblock}{}
    \begin{center}
      \texttt{substitute-keyword-arguments} substitutes keyword arguments
    \end{center}
  \end{exampleblock}

  \vspace{-0.1cm}
  \begin{lstlisting}[language=Lisp]
    (arguments
     (substitute-keyword-arguments (package-arguments hdf4)
       ((#:configure-flags flags) `(cons* "--disable-netcdf" ,flags))))
    ;; seen in package hdf4-alt
   \end{lstlisting}

   \vspace{-0.1cm}
   \begin{block}{}
     \vspace{-0.2cm}
     % \visible<2>{
     \begin{enumerate}
     \item<2->
     \begin{lstlisting}[language=Lisp]
scheme@(guix-user)> ,use(srfi srfi-1)
scheme@(guix-user)> ,pp (lset-difference equal?
          (substitute-keyword-arguments (package-arguments hdf4)
            ((#:configure-flags flags) `(cons* "--disable-netcdf" ,flags)))
          (package-arguments hdf4))
$1 = ((cons* "--disable-netcdf" (list "--enable-shared" "FCFLAGS=-fallow-argument-mismatch"
                                      "FFLAGS=-fallow-argument-mismatch"
                                      "--enable-hdf4-xdr")))
     \end{lstlisting}
     \end{enumerate}
     % }
   \end{block}

\end{frame}



\thisslide{assoc-ref}
\begin{frame}<presentation>[fragile, label=assoc-ref]{Association list}
  \begin{center}
    (\emph{alist}) association list = list of pairs \code{(this . that)}

    \bigskip

    think: \code{(list (key1 . value1) (key2 . value2) ...)}
  \end{center}
  \begin{block}{}
    \begin{enumerate}
    \item Type
      \begin{lstlisting}[language=Lisp]
 scheme@(guix-user)> (define alst (list '(a . 1) '(2 . 3) '("foo" . v)))
 scheme@(guix-user)> (assoc-ref alst "foo")
 scheme@(guix-user)> (assoc-ref alst 'a)
      \end{lstlisting}
    \item Type
    \begin{lstlisting}[language=Lisp]
      scheme@(guix-user)> (assoc-ref (package-inputs hi) "gawk")
    \end{lstlisting}
  \end{enumerate}
  \end{block}
\end{frame}


\thisslide{example-freedgnuplot}
\begin{frame}<presentation>[fragile, label=example-freedgnuplot]{Ready?%
    \hfill seen in package \texttt{feedgnuplot}}
  \vspace{-0.3cm}
  \begin{lstlisting}[language=Lisp]
     1	(add-after 'install 'wrap
     2	  (lambda* (#:key inputs outputs #:allow-other-keys)
     3	    (let* ((out (assoc-ref outputs "out"))
     4	           (gnuplot (search-input-file inputs "/bin/gnuplot"))
     5	           (modules '("perl-list-moreutils" "perl-exporter-tiny"))
     6	           (PERL5LIB (string-join
     7	                      (map (lambda (input)
     8	                             (string-append (assoc-ref inputs input)
     9	                                            "/lib/perl5/site_perl"))
    10	                           modules)
    11	                      ":")))
    12	      (wrap-program (string-append out "/bin/feedgnuplot")
    13	        `("PERL5LIB" ":" suffix (,PERL5LIB))
    14	        `("PATH" ":" suffix (,(dirname gnuplot)))))))
  \end{lstlisting}
\end{frame}


\begin{frame}[plain]{}

  \begin{center}
    Let build something!
  \end{center}

  \vfill

  \begin{flushright}
    Before, we need to fetch something, isn't it?
  \end{flushright}
\end{frame}

\section{\texttt{origin} field}


\thisslide{origin}
\begin{frame}<presentation>[fragile, label=origin]{\texttt{origin} field%
  \hfill seen in package \texttt{feedgnuplot}}
  \begin{exampleblock}{}
    \begin{lstlisting}[language=Lisp]
  (source (origin
      (method git-fetch)
      (uri (git-reference
            (url home-page)
            (commit (string-append "v" version))))
      (file-name (git-file-name name version))
      (sha256
       (base32
        "0403hwlian2s431m36qdzcczhvfjvh7128m64hmmwbbrgh0n7md7"))))
    \end{lstlisting}
  \end{exampleblock}
\end{frame}



\thisslide{def-origin}
\begin{frame}<presentation>[fragile, label=def-origin]%
  {Defining \texttt{origin}}
  \vspace{-0.2cm}
  \begin{description}
  \item[origin] Object representing a source code (data) (Scheme record)
    \begin{description}
    \item[method] How to fetch
    \item[uri] Where to fetch
    \item[sha256] Checksum, see \code{guix hash}
    \end{description}
  \end{description}

  \vfill
  \begin{block}{}
    \begin{enumerate}
    \item Remember the package \code{hi}
      \begin{lstlisting}[language=Lisp]
        scheme@(guix-user)> (package-source hi)
      \end{lstlisting}
    \item Compare \code{feedgnuplot} (from module \code{(gnu packages maths)})
      \begin{lstlisting}[language=Lisp]
        scheme@(guix-user)> (package-source feedgnuplot)
      \end{lstlisting}
    \item Another terminal, compare
\begin{verbatim}
$ guix build -L examples/packages hi --source
$ guix build -L examples/packages feedgnuplot --source
\end{verbatim}
    \end{enumerate}
  \end{block}
\end{frame}



\thisslide{def-origin}
\begin{frame}<presentation>[fragile, label=def-origin]%
  {Defining \texttt{origin} II
  \hfill more \code{method}}
\begin{alertblock}{}
  \begin{center}
    \textbf{fixed-output derivation} = content known in advance
  \end{center}
\end{alertblock}

  \vfill
  \begin{exampleblock}{}
    \begin{itemize}
    \item<1-> \code{url-fetch} fetches data from URL (= a string or a list of
      strings)

      \hfill module \code{(guix download)}
    \item<2-> \code{git-fetch} fetches a \code{git-reference} object
      \hfill module \code{(guix git-download)}
    \item<3-> \code{hg-fetch} fetches a \code{hg-reference} object
      \hfill module \code{(guix hg-download)}
    \item<3-> \code{svn-fetch} fetches a \code{svn-reference} object
      \hfill module \code{(guix svn-download)}
    \item<3-> etc.
    \end{itemize}
  \end{exampleblock}

  \vfill
  \begin{center}
    see \href{https://guix.gnu.org/manual/devel/en/guix.html\#origin-Reference}%
    {\code{origin} Reference (link)} in Guix manual
  \end{center}
\end{frame}



\thisslide{modify-origin-1}
\begin{frame}<presentation>[fragile, label=modify-origin-1]%
  {Modifying \texttt{origin}}
  \vspace{-0.2cm}
  \begin{block}{}
    \begin{enumerate}
    \item<1-> Fetch the source code of the package \code{gecode}
\begin{verbatim}
$ guix build gecode --source
\end{verbatim}
    \item<2-> Show what \code{git-fetch} does behind the scene
\begin{verbatim}
$ ./examples/scripts/show-me-fetch.scm gecode
$ eval $(./examples/scripts/show-me-fetch.scm gecode)
\end{verbatim}
    \item<3-> Compare both
\begin{verbatim}
$ diff -rq /tmp/gecode $(guix build gecode -S)
\end{verbatim}
    \end{enumerate}
  \end{block}

  \vfill
  \visible<4>{
  \begin{alertblock}{}
    \begin{center}
      how to remove these files?
    \end{center}
  \end{alertblock}
  }
\end{frame}



\thisslide{modify-origin-2}
\begin{frame}<presentation>[fragile, label=modify-origin-2]%
  {Modifying \texttt{origin} II}
  \vspace{-0.2cm}
  \begin{block}{}
    \begin{enumerate}
    \item Compare both
\begin{verbatim}
$ diff -rq /tmp/gecode $(guix build gecode -S)
\end{verbatim}
    \item Another comparison
\begin{verbatim}
$ guix hash -S nar -f base32-nix -H sha256 -x $(guix build -S gecode)
$ guix hash -S nar -f base32-nix -H sha256 -x /tmp/gecode

$ rm /tmp/gecode/gecode/kernel/var-{imp,type}.hpp
$ guix hash -S nar -f nix-base32 -H sha256 -x /tmp/gecode
\end{verbatim}
    \end{enumerate}
  \end{block}

  \vfill
  \begin{alertblock}{}
    \begin{center}
      how to remove these files?
    \end{center}
  \end{alertblock}
\end{frame}



\thisslide{modify-origin-2}
\begin{frame}<presentation>[fragile, label=modify-origin-2]%
  {Modifying \texttt{origin} III
    \hfill snippet}
  \vspace{-0.3cm}
\begin{center}
  A S-expression (or G-expression) that will be run in the source directory
\end{center}

\begin{lstlisting}[language=Lisp]
(origin
...
  (snippet
    '(begin
       ;; delete generated sources
       (for-each delete-file
                 '("gecode/kernel/var-imp.hpp"
                   "gecode/kernel/var-type.hpp"))))
\end{lstlisting}

\vfill
\begin{block}{}
  \begin{center}
    \code{patches} vs \code{snippet}: it depends on
  \end{center}
\end{block}
Look at the module \texttt{(guix builds utils)} for helpers as
\code{delete-file-recursively}, etc.
\end{frame}

\section{Arguments}


\thisslide{arguments}
\begin{frame}<presentation>[fragile, label=arguments]%
  {Pass \texttt{arguments} to the build system}
\begin{lstlisting}[language=Lisp]
 (arguments
  (list #:configure-flags
        #~(list "--enable-dynamic-build"
                #$@(if (target-x86?)
                       #~("--enable-vector-intrinsics=sse")
                       #~())
                "--enable-ldim-alignment")
        #:make-flags #~(list "FC=gfortran -fPIC")
        #:phases
        #~(modify-phases %standard-phases
\end{lstlisting}

\vfill
\begin{alertblock}{}
  \begin{center}
    \code{\#:configure-flags} is keyword.\\

    What is \code{\#\~{}} or \code{\#\$@}?
  \end{center}
\end{alertblock}
\end{frame}


\thisslide{g-expression}
\begin{frame}<presentation>[fragile, label=g-expression]%
  {G-expression}
  \begin{center}
    {Remember quasiquote and unquote?}

    \begin{tabular}{clcll}
      \code{\#\~{}}&is similar as&\code{\`{}}&with context
      &(host machine, store state, etc.)\\
      \code{\#\${}}&is similar as&\code{,}&with context\\
      \code{\#\$@{}}&is similar as&\code{,@}&with context
    \end{tabular}
  \end{center}

  \vfill
  \begin{block}{}
    \begin{center}
      \code{\#\~{}(string-append \#\$hello "/some/string")}

      ``means''

      \code{"/gnu/store/8bzzc70vgzdvj6qdzhdpd709m4y2kw7z-hello-2.12.1/some/string"}
    \end{center}
  \end{block}

\vfill
\begin{center}
  {https://simon.tournier.info/posts/2023-11-02-gexp-intuition.html}
\end{center}
\end{frame}



\thisslide{g-expression-2}
\begin{frame}<presentation>[fragile, label=g-expression-2]%
  {G-expression II}
\begin{lstlisting}[language=Lisp]
(replace 'install
  (lambda* (#:key outputs #:allow-other-keys)
    (mkdir-p (string-append #$output "/bin"))
    (chmod "BQN" #o755)
    (rename-file "BQN" "bqn")
    (install-file "bqn" (string-append #$output "/bin"))))
\end{lstlisting}
\end{frame}


\subsection*{\texttt{\%standard-phases}}

Except one, all package build systems implement a notion of
\hrefsf{https://guix.gnu.org/manual/devel/en/guix.html\#Build-Phases}%
{Build Phases}: a sequence of actions that the build system executes, when you
build the package.  For instance, these actions might be \texttt{unpack},
\texttt{configure}, \texttt{build}, \texttt{check}, \texttt{install}, etc.


\subsubsection{List and compare \texttt{\%standard-phases}}

For each build system, this sequence of actions is stored by
\texttt{\%standard-phases}.  Please, note that this sequence of actions is
build system depends, i.e., \texttt{\%standard-phases} is defined per build
system.  For example, the \texttt{\%standard-phases} for the GNU build system
is defined by the module \texttt{(guix build-system gnu)}.  And the
\texttt{\%standard-phases} for the Python build system is defined by the
module \texttt{(guix build-system python)}.

\medskip

It is possible to interactively explore these sequences of actions using
\texttt{guix repl}.  Here, we load the GNU build system
\texttt{\%standard-phases}, and we rebind that variable to
\texttt{standard-phases} (without the percent \texttt{\%}) and prefix it with
\texttt{gnu:}

\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> (use-modules ((guix build gnu-build-system)
  #:select ((%standard-phases . standard-phases))  ;also rename
  #:prefix gnu:))
\end{lstlisting}

Similarly, let load the sequence of actions for the Python build system,

\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> (use-modules ((guix build python-build-system)
  #:select ((%standard-phases . standard-phases))
  #:prefix python:))
\end{lstlisting}

And using \texttt{lset-difference} from the module \texttt{(srfi srfi-1)}, it
is straightforward to list the items that are part of the GNU build system but
not part of the Python build system.

\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> ,use(srfi srfi-1)
scheme@(guix-user)> ,pp (lset-difference eq?
                          gnu:standard-phases python:standard-phases)
\end{lstlisting}

Based on that, we are able to compare the sequence of actions
(\texttt{\%standard-phases}) for two build systems.  And we could have a
simple script for automating:

\begin{verbatim}
$ ./examples/scripts/compare-bs-phases.scm gnu python

Compare %standard-phases of "gnu" and "python":
  22 phases in "gnu"
  28 phases in "python"
  20 phases in common

Only in "gnu":
 + bootstrap
 + configure

Only in "python":
 + ensure-no-mtimes-pre-1980
 + enable-bytecode-determinism
 + ensure-no-cythonized-files
 + add-install-to-pythonpath
 + add-install-to-path
 + wrap
 + sanity-check
 + rename-pth-file
\end{verbatim}

The manual provides some details about the meaning of various phases, see
\hrefsf{https://guix.gnu.org/manual/devel/en/guix.html\#Build-Systems}%
{Build Systems}.



\thisslide{phases}
\begin{frame}<presentation>[fragile, label=phases]%
  {Phases \code{\%standard-phases}}
  \begin{center}
    \code{\%standard-phases} is defined build system by build system
  \end{center}

  \vfill
\begin{verbatim}
$ ./examples/scripts/compare-bs-phases.scm gnu python
\end{verbatim}

  \vfill
  \begin{tabular}{c|c}
    \visible<2->{
    \begin{minipage}{0.5\textwidth}
      \begin{description}
      \item[Phases]
        \begin{enumerate}
        \item \code{'configure}
        \item \code{'build}
        \item \code{'install}
        \item \code{'check}
        \item etc.
        \end{enumerate}
      \end{description}
    \end{minipage}
    }
    &
      \visible<3->{
      \begin{minipage}{0.5\textwidth}
        \begin{description}
        \item[Actions]
          \begin{enumerate}
          \item \code{replace}
          \item \code{delete}
          \item \code{add-before}
          \item \code{add-after}
          \end{enumerate}
        \end{description}
      \end{minipage}
      }
  \end{tabular}
\end{frame}



\thisslide{phases-2}
\begin{frame}<presentation>[fragile, label=phases-2]%
  {Phases \code{\%standard-phases} II}
\begin{lstlisting}[language=Lisp]
(arguments
 (list
  #:phases
  #~(modify-phases %standard-phases
      (delete 'configure)
      (add-before 'build 'set-prefix-in-makefile
        (lambda* (#:key inputs #:allow-other-keys)
          (substitute* "Makefile"
            (("PREFIX =.*")
             (string-append "PREFIX = " #$output "\n"))
            (("XMLLINT =.*")
             (string-append "XMLLINT = "
                            (search-input-file inputs "/bin/xmllint")
                            "\n"))))))))))
;; see in Guix manual
\end{lstlisting}
\end{frame}


\thisslide{phases-3}
\begin{frame}<presentation>[fragile, label=phases-3]%
  {Phases \code{\%standard-phases} III}

  \begin{lstlisting}[language=Lisp]
(arguments
  (if (not (target-x86-64?))
      ;; This test is only broken when using openblas, not openblas-ilp64.
      (list
        #:phases
        #~(modify-phases %standard-phases
            (add-after 'unpack 'adjust-tests
              (lambda _
                (substitute* "test/test_layoutarray.jl"
                  (("test all\\(B") "test_broken all(B"))))))
      '()))
;; see in julia-arraylayouts
    \end{lstlisting}
\end{frame}


\section{Questions}


\thisslide{resource}
\begin{frame}<presentation>[fragile, label=resource]{Resources (links)}
  \begin{description}
  \item[Talk] «
    \href{https://10years.guix.gnu.org/video/guixy-guile-the-derivation-factory-a-tour-of-the-guix-source-tree/}%
    {A tour of the Guix source tree} » (video 40min)
  \item[Talk] «
    \href{https://archive.fosdem.org/2020/schedule/event/gexpressionsguile/}%
    {Introduction to G-Expressions} » (video 30min)

    \vfill
    \begin{center}
      \rule{0.5\textwidth}{0.4pt}

      \textbf{self-promotion}

      \href{https://simon.tournier.info/posts/}%
      {\texttt{https://simon.tournier.info/posts/}}
    \end{center}
    \vfill

  \item[Post] « \href{https://simon.tournier.info/posts/2022-11-28-guile-dual-numbers.html}%
    {Automatic differentiation by dual numbers using Guile} »
  \item[Post] « \href{https://simon.tournier.info/posts/2023-11-02-gexp-intuition.html}%
    {From naive to rough intuition about G-expression} »
  \item[Post] « \href{https://simon.tournier.info/posts/2023-11-01-gexp.html}%
    {Quasiquote and G-expression: Fibonacci sequence using derivations} »
  \end{description}
\end{frame}



\thisslide{thanks}
\begin{frame}<presentation>[label=thanks]{Packaging = practise and practise again}
  If I might,
  \medskip
  \begin{enumerate}
  \item Dive into existing packages and deal with Guix manual and community.
  \item Most of the ``tricks'' is about \uline{\textbf{a lot of}} practise.
    Quoting \texttt{rekado},

    \vfill
    \begin{flushright}
      I wish I had anything to say about this other than:

      \emph{“try again, give up, forget about it, remember it, ask for
        pointers, repeat”}

      \href{https://logs.guix.gnu.org/guix-hpc/2023-10-13.log\#142159}%
      {\textsf{\texttt{\#guix-hpc} on 2023-10-13}}.
    \end{flushright}
  \end{enumerate}
  \vfill
  \begin{center}
    \uline{\emph{do not forget that}~\textbf{packaging is a craft}}
  \end{center}
\end{frame}
