#!/usr/bin/env bash

for ext in vrb aux log nav out snm toc rubbercache;
do
    find src/ -name "*.${ext}" -type f -print -exec rm {} \;
done
